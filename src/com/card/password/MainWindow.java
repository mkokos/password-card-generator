package com.card.password;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class MainWindow {
    private JPanel panel1;
    private JTextArea textArea1;
    private JButton button1;
    private JCheckBox aZCheckBox;
    private JCheckBox aZCheckBox1;
    private JCheckBox a09CheckBox;
    private JTextField dodatkowetextField1;
    private JTextField znakowWLiniTextField;
    private JTextField iloscLiniTextField1;

    private JButton odtworzButton;
    private JTextField REFTextField;
    private JCheckBox znakCheckBox;
    private JCheckBox dodatkoweZnakiCheckBox;
    public MainWindow() {
        button1.addActionListener(e -> {
            textArea1.setText("");
            LineGenerator linia=new LineGenerator();

            if(a09CheckBox.isSelected()){
                linia.dodajcyfry();
            }
            if(aZCheckBox.isSelected()){
                linia.dodajdlitery();
            }
            if(aZCheckBox1.isSelected()){
                linia.dodajmlitery();
            }
            if(znakCheckBox.isSelected()){
                linia.dodajznaki();
            }
//                if (dodatkoweZnakiCheckBox.isSelected())
//                {
//                    try {
//                        dodatkowetextField1.setBackground(Color.white);
//                        linia.dodajtekst(dodatkowetextField1.getText());
//
//                    } catch (RuntimeException r) {
//                        if (r.getMessage() == "zawiera") {
//                            dodatkowetextField1.setBackground(Color.red);
//                        }
//                    }
//                }

            linia.setDlugosclini(Integer.parseInt(znakowWLiniTextField.getText()));
            try {
                znakowWLiniTextField.setBackground(Color.white);
                linia.setDlugosclini(Integer.parseInt(znakowWLiniTextField.getText()));
            }catch(NumberFormatException nfe){
                znakowWLiniTextField.setBackground(Color.red);
                linia.setDlugosclini(0);
            }

            long losowa=Math.abs(new Random().nextLong());
            //debshow("refferencja= "+reff+"\n");
            //debshow(""+Long.MAX_VALUE);
            if(losowa%10==0)
            {
                losowa=losowa+1;
            }
            if (losowa<100000L)
                losowa=losowa+100000L;

            REFTextField.setText(Reference.getref(losowa,aZCheckBox1.isSelected(),aZCheckBox.isSelected(),znakCheckBox.isSelected(),a09CheckBox.isSelected(),znakowWLiniTextField.getText(), iloscLiniTextField1.getText(),true));
            try{
                iloscLiniTextField1.setBackground(Color.white);
            for (int i = 0; i<Integer.parseInt(iloscLiniTextField1.getText()); i++)
            {
                textArea1.append(linia.generujlinie(losowa));
                textArea1.append("\n");

                linia.clearLinia();
                losowa++;
            }}
            catch (NumberFormatException nfe){
                iloscLiniTextField1.setBackground(Color.red);


            }
        });

        odtworzButton.addActionListener(e -> {
            Reference ref=new Reference();
            ref.calcRef(REFTextField.getText(),true);
            ref.odtworz(textArea1);
        });
        znakowWLiniTextField.addActionListener(e -> {


        });
        znakowWLiniTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if(Integer.parseInt(znakowWLiniTextField.getText())>80) {
                    znakowWLiniTextField.setText("80");
            }}
        });
        iloscLiniTextField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if(Integer.parseInt(iloscLiniTextField1.getText())>80) {
                    iloscLiniTextField1.setText("80");
                }
            }
        });
        textArea1.addMouseListener(new MouseAdapter() {
        });
        textArea1.addKeyListener(new KeyAdapter() {
        });
        textArea1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //super.mouseClicked(e);
                if (e.getButton() != MouseEvent.NOBUTTON){
                    textArea1.selectAll();
                    textArea1.copy();
                };
            }
        });
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("GENERATOR KARTY HASEŁ");
        frame.setContentPane(new MainWindow().panel1);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);




    }
}


