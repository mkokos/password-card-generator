package com.card.password;

 public  class Debug {
    private boolean endebug;
    public Debug(){
        endebug=false;
    }
    public Debug(boolean x){
        endebug=x;
    }
    public void debstart(){
        endebug=true;
    }
    public void debstop(){
        endebug=false;
    }

     public static void debshow(String mess) {
         debshow(mess,true);

     }
     public static void debshow(String mess, boolean endebug) {
         if (endebug) {
             System.out.println(mess);
         }
     }
}
