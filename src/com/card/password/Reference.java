package com.card.password;

import javax.swing.*;
import java.math.BigInteger;


//dlit 1 bit
//mlit 1 bit
//zn   1 bit
//cyf  1 bit
//
//
//
//
public class Reference {
    private boolean uppercase;
    private boolean leters;
    private boolean signs;
    private long seed;
    private boolean
            digits;
    private int numOfLines;
    private int lenght;

    Reference() {
    }
    public static String getref(long losowa,boolean mlitery,boolean dlitery,boolean znaki,boolean cyfry,String dlugosc,String ilosc){
        return getref(losowa,mlitery,dlitery,znaki,cyfry,dlugosc,ilosc,false);
    }

    static String getref(long seed, boolean leters, boolean upercase, boolean signs, boolean digits, String lenght, String numOfLines, boolean debug){
        StringBuilder referencja=new StringBuilder();
        if (leters) {
            referencja.append("1");
        } else {
            referencja.append("0");
        }
        if (upercase) {
            referencja.append("1");
        } else {
            referencja.append("0");
        }
        if (digits) {
            referencja.append("1");
        } else {
            referencja.append("0");
        }
        if (signs) {
            referencja.append("1");
        } else {
            referencja.append("0");
        }
        Debug.debshow("w binarnym= "+referencja.toString(),debug);
        String temp= SystemChange.changeSystem(referencja.toString(),2,10,debug);
        referencja.setLength(0);
        if (temp.length()<2)
            referencja.append("0");
        referencja.append(temp);
        if (lenght.length() < 2) {
            referencja.append("0");}
            referencja.append(lenght);
        if (numOfLines.length()<2){
        referencja.append("0");}
        referencja.append(numOfLines);
        Debug.debshow("do zakodowania"+referencja.toString(),debug);
        int tmpint=Integer.parseInt(referencja.toString());
        String strlos=""+seed;
        Debug.debshow("seed= "+seed,debug);
        StringBuilder tmps=new StringBuilder("9");
        strlos=strlos.substring(0,6);
        Debug.debshow("do dodania= "+strlos,debug);
        tmps.append(strlos);
        Debug.debshow("to="+tmpint,debug);
        if (Integer.parseInt(tmps.toString())%10==0) {
            tmps.deleteCharAt(tmps.length()-1);
            tmps.append("1");}
        Debug.debshow("plus to="+tmps,debug);
        tmpint=Integer.parseInt(tmps.toString())+tmpint;
        referencja.setLength(0);
        if(tmpint%10==0){
            tmpint=tmpint+1;
        }
        Debug.debshow("suma="+tmpint,debug);
        referencja.append(tmpint);
        String tmp= SystemChange.changeSystem(referencja.toString(),10,88,false);
        referencja.setLength(0);
        referencja.append(tmp);
        String tmp2=""+seed;
        tmp2= SystemChange.changeSystem(tmp2,10,88,false);
        referencja.append(tmp2);
        Debug.debshow("#ref= "+referencja.toString(),debug);
        Debug.debshow("koniec ref",debug);
        return referencja.toString();
    }

    private void setSeed(long seed) {
        this.seed = seed;
    }

    private void setDigits(boolean digits) {
        this.digits = digits;
    }

    private void setLenght(int lenght) {
        this.lenght = lenght;
    }

    private void setNumOfLines(int numOfLines) {
        this.numOfLines = numOfLines;
    }

    private void setLeters(boolean leters) {
        this.leters = leters;
    }

    private void setSigns(boolean signs) {
        this.signs = signs;
    }

    private void setUppercase(boolean uppercase) {
        this.uppercase = uppercase;
    }



    public void calcRef(String ref){
        calcRef(ref,false);
    }
    public void calcRef(String ref, boolean debug){
        StringBuilder stringBuilder=new StringBuilder("9");
        stringBuilder.append(SystemChange.toDEC(ref.substring(4,ref.length()),88,false).substring(0,6));
        if (Integer.parseInt(stringBuilder.toString())%10==0) {
           stringBuilder.deleteCharAt(stringBuilder.length()-1);
           stringBuilder.append("1");}
        int inttemp=Integer.parseInt(stringBuilder.toString());
        Debug.debshow("suma="+ SystemChange.toDEC(ref.substring(0,4),88,false),debug);
        Debug.debshow("minus to"+inttemp,debug);
        inttemp=Math.abs(inttemp-Integer.parseInt(SystemChange.toDEC(ref.substring(0,4),88,false)));
        stringBuilder.setLength(0);
        stringBuilder.append(SystemChange.toDEC(ref.substring(4,ref.length()),88,false));
        Debug.debshow("wychodzi=="+inttemp,debug);
        Debug.debshow("seed=== "+stringBuilder.toString(),debug);
        setSeed(new BigInteger(stringBuilder.toString()).longValue());
//        setSeed(Long.parseLong(stringBuilder.toString()));
        stringBuilder.setLength(0);
        stringBuilder.append(inttemp);
        StringBuilder tmpstr=new StringBuilder("");
        if (stringBuilder.length()<6) {
            setNumOfLines(Integer.parseInt("" + stringBuilder.charAt(3) + stringBuilder.charAt(4)));
            setLenght(Integer.parseInt("" + stringBuilder.charAt(1) + stringBuilder.charAt(2)));
            tmpstr.append(stringBuilder.substring(0,1));
        }   else {
            Debug.debshow("\n\ntu\n\n",debug);
            setNumOfLines(Integer.parseInt("" + stringBuilder.charAt(4) + stringBuilder.charAt(5)));
            setLenght(Integer.parseInt("" + stringBuilder.charAt(2) + stringBuilder.charAt(3)));
            tmpstr.append(stringBuilder.substring(0,2));
        }
        stringBuilder=tmpstr;
        Debug.debshow("po przetworzeniu="+inttemp,debug);
        Debug.debshow("ile="+ numOfLines,debug);
        Debug.debshow("dlu" + lenght,debug);
        Debug.debshow("zostalo="+stringBuilder,debug);
        Debug.debshow("\n\n");
        stringBuilder=new StringBuilder(SystemChange.decToSystem(stringBuilder.toString(),2,debug));
        Debug.debshow("\n!!!!!!!\n");
        while (stringBuilder.length()<4){
            stringBuilder.insert(0,"0");
        }
            Debug.debshow("w 2="+stringBuilder.toString(),debug);
        if (stringBuilder.charAt(0)=='1'){
            setLeters(true);
        }else
            setLeters(false);
        if (stringBuilder.charAt(1)=='1')
            setUppercase(true);
        else
            setUppercase(false);
        if (stringBuilder.charAt(2)=='1')
            setDigits(true);
        else
            setDigits(false);
        if (stringBuilder.charAt(3)=='1')
            setSigns(true);
        else
            setSigns(false);






    }
 void odtworz(JTextArea textArea1){
        odtworz(textArea1,true);
    }
void odtworz(JTextArea textArea1, boolean debug){
    LineGenerator linia=new LineGenerator();

    textArea1.setText("");
    if(digits){
        linia.dodajcyfry();
    }
    if(uppercase){
        linia.dodajdlitery();
    }
    if(leters){
        linia.dodajmlitery();
    }
    if(signs){
        linia.dodajznaki();
    }
    linia.setDlugosclini(lenght);
    for (int i = 0; i< numOfLines; i++)
    {
        textArea1.append(linia.generujlinie(seed));
        textArea1.append("\n");
        linia.clearLinia();
        seed++;



    }
    Debug.debshow("koniec odt",debug);
}

}
