package com.card.password;

import java.util.Random;

public class LineGenerator extends AlowedSigns {
    private int dlugosclini;
    private String linia="";
    private String wzorzec;
    private Random generator;
    private boolean debug=false;

    LineGenerator() {

       dlugosclini=20;
       wzorzec=getSzablon();
       generator = new Random();
   }
   public String generujlinie(long arg) {
       wzorzec = getSzablon();
       generator.setSeed(arg);
       if (wzorzec.length() > dlugosclini) {
           for (int i = 0; i < dlugosclini; i++) {

               int losowa = generator.nextInt(wzorzec.length());
               linia= linia + wzorzec.charAt(losowa);

               debshow("wylosowany znak" +wzorzec.charAt(losowa),debug );
               wzorzec=new StringBuffer(wzorzec).deleteCharAt(losowa).toString();
               debshow("rozdzielony \n"+wzorzec,debug);
           }
           return linia;
       } else{
           debshow(" ",debug);
           return null;
       }
   }
    public void clearLinia(){
       linia="";
    }
    public void setDlugosclini(int dlugosclini) {
        this.dlugosclini = dlugosclini;
    }

}
