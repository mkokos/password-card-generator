package com.card.password;

public class AlowedSigns extends Debug {

    private String mlitery = "abcdefghijklmnopqrstuvwxyz"; //26
    private String dlitery = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private String mdlitery = mlitery + dlitery;
    private String szablon;

    public AlowedSigns() {
        szablon = "";
    }

    public void dodajtekst(String tekst) throws RuntimeException {
        if (szablon.contains(tekst)) {
            debshow("Zawiera" + tekst);
            throw new RuntimeException("zawiera");
        } else {
            szablon = szablon + tekst;
        }
    }

    public void dodajznaki() throws RuntimeException {
        String znaki = "!@#$%^&*(){}[]\\|:\";'<>?,./";
        szablon = szablon + znaki;
    }

    public void dodajmlitery() {

        szablon = szablon + mlitery;

    }

    public void dodajdlitery() {

        szablon = szablon + dlitery;

    }

    public void dodajcyfry() {

        String cyfry = "0123456789";
        szablon = szablon + cyfry;

    }

    public void dodajmdlitery() {
        szablon = szablon + mdlitery;
    }

    public void setSzablon(String szablon) {
        this.szablon = szablon;
    }


    public String getSzablon() {
        return szablon;
    }
}
