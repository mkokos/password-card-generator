package com.card.password;


import java.math.BigInteger;

public class SystemChange {
    private static final String CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*(){}[]\\|:\";'<>?,./";

    public SystemChange() {
    }

    static String changeSystem(String sliczba, Integer zSystem, Integer nSystem, boolean debug) {
        return decToSystem(toDEC(sliczba, zSystem, debug), nSystem, debug);
    }

    public static String toDEC(String sliczba, int zSystem, boolean debug) {
        new BigInteger("0");
        BigInteger bigInteger;
        Debug.debshow("przed konw " + sliczba, debug);
        sliczba = new StringBuilder(sliczba).reverse().toString();
        if (zSystem < 10) {
            bigInteger = BigInteger.ZERO;
            for (int i = 0; i < sliczba.length(); i++) {
                BigInteger tmp = new BigInteger("" + zSystem);
                tmp = tmp.pow(i);
                tmp = tmp.multiply(new BigInteger("" + sliczba.charAt(i)));
                bigInteger = bigInteger.add(tmp);
            }
        } else if (zSystem == 10) {
            bigInteger = new BigInteger(sliczba);
        } else {
            bigInteger = BigInteger.ZERO;
            for (int i = 0; i < sliczba.length(); i++) {
                Debug.debshow("akt licznik =" + i + " znak=" + sliczba.charAt(i) + " potega=" + (int) Math.pow(zSystem, i) + " wynik=" + bigInteger, false);
                BigInteger tmp = new BigInteger("" + zSystem);
                Debug.debshow("podstawa=" + tmp, debug);
                tmp = tmp.pow(i);
                Debug.debshow("potega=" + tmp, debug);
                tmp = tmp.multiply(new BigInteger("" + CHARS.indexOf(sliczba.charAt(i))));
                Debug.debshow("wynik=" + tmp, debug);
                bigInteger = bigInteger.add(tmp);
            }
            StringBuilder stringBuilder = new StringBuilder(bigInteger.toString());
            stringBuilder.reverse();
            bigInteger = new BigInteger(stringBuilder.toString());
        }
        return bigInteger.toString();
    }

    public static String decToSystem(String sliczba, Integer nSystem, boolean debug) {
        BigInteger bigInteger = new BigInteger(sliczba);
        StringBuilder wynik = new StringBuilder();
        while (bigInteger.compareTo(BigInteger.ZERO) > 0) {
            BigInteger temp = bigInteger.remainder(new BigInteger(nSystem.toString()));
            wynik.append(SystemChange.CHARS.charAt(temp.remainder(new BigInteger(nSystem.toString())).intValue()));
            bigInteger = bigInteger.divide(new BigInteger(nSystem.toString()));
        }
        Debug.debshow("wynik=" + wynik.toString() + " sys=" + nSystem.toString(), debug);
        wynik = wynik.reverse();
        return wynik.toString();
    }
}
